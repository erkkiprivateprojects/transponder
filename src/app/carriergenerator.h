#ifndef CARRIERGENERATOR_H
#define CARRIERGENERATOR_H

#include <QByteArray>
#include <QIODevice>
#include <QObject>
#include <QAudioFormat>
#include <QVector>
#include <QElapsedTimer>

class CarrierGenerator : public QIODevice
{
    Q_OBJECT
public:
    CarrierGenerator(const QAudioFormat &format, qint64 durationUs, int sampleRate, QObject *parent);

    void start();
    void stop();

    qint64 readData(char *data, qint64 len) override;
    qint64 writeData(const char *data, qint64 len) override;
    qint64 bytesAvailable() const override;

    void sendAudioData(QVector<qint16> audioData);

private:
QByteArray performAudioMix(int bufferSize);
    QByteArray m_buffer;

    QAudioFormat mCurrentFormat;
    QElapsedTimer mReadInterval;

    QVector<qint16> mAudioToBeMixed;

    QByteArray mDataBlock;
};

#endif // CARRIERGENERATOR_H
