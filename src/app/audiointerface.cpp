#include <QDebug>
#include <QAudioDeviceInfo>
#include <QtEndian>
#include <QDateTime>
#include "audiointerface.h"
#include "audioconfiguration.h"

#include "DspFilters/ChebyshevI.h"

const int bufferSizeDivisor = 16;

AudioInterface::AudioInterface(QObject *parent): QObject(parent)
  ,   m_outputDevice(QAudioDeviceInfo::defaultOutputDevice())
  ,   m_inputDevice(QAudioDeviceInfo::defaultInputDevice())
  ,   m_outputCarriedGenerator(0)
  ,   m_audioInput(0)
  ,   m_audioOutput(0)
  ,   m_inputAudioDataReceiver(0)
  ,   m_output(0)
  ,   mRxAudioTimeMs(0)
  ,   mTxAudioTimeMs(0)
  ,   mFilterBandPass(NULL)
  ,   mFilterLowPass(NULL)
  , EnvIn(0)
{

}

bool AudioInterface::initAudio()
{
    m_pushTimer = new QTimer;
    initializeAudio();

    return true;
}

void AudioInterface::onSendAudioData(QVector<qint16> audioData)
{
    /* Data is signed 16 bit with current sample rate */

    /* this data should be mixed to current current stream */
    m_outputCarriedGenerator->sendAudioData(audioData);
    m_audioOutput->start(m_outputCarriedGenerator);
}

void AudioInterface::deviceChanged()
{
    m_pushTimer->stop();
    m_outputCarriedGenerator->stop();
    m_inputAudioDataReceiver->stop();

    m_audioOutput->stop();
    m_audioOutput->disconnect(this);

    m_audioInput->stop();
    m_audioInput->disconnect(this);

    initializeAudio();
}

void AudioInterface::volumeChanged(int value)
{
    if (m_audioOutput) {
        qreal linearVolume =  QAudio::convertVolume(value / qreal(100),
                                                    QAudio::LogarithmicVolumeScale,
                                                    QAudio::LinearVolumeScale);

        m_audioOutput->setVolume(linearVolume);
    }
}

void AudioInterface::onRxAudioNotify()
{
    if( mRxAudioTimeMs==0) {
        mRxAudioWallTime=QDateTime::currentDateTime().addMSecs(-1*m_audioInput->notifyInterval());
    }

    mRxAudioTimeMs+=m_audioInput->notifyInterval();
    //qDebug() << "rxTimeMs" <<mRxAudioTimeMs <<m_audioInput->notifyInterval()<<mRxAudioWallTime.msecsTo(QDateTime::currentDateTime());
}

void AudioInterface::onTxAudioNotify()
{
    if( mTxAudioTimeMs==0) {
        mTxAudioWallTime=QDateTime::currentDateTime().addMSecs(-1*m_audioOutput->notifyInterval());
    }
    mTxAudioTimeMs+=m_audioOutput->notifyInterval();
    //qDebug() << "txTimeMs" <<mTxAudioTimeMs <<m_audioOutput->notifyInterval()<<mTxAudioWallTime.msecsTo(QDateTime::currentDateTime());

}

void AudioInterface::onNewAudioDataReceived(QVector<qint16> audioData)
{
    /* Perform possible digital filtering here */
    if ( mFilterBandPass!=NULL && mFilterLowPass!=NULL){

        int numSamples =audioData.size();

    float* audio[1];
    audio[0] = new float[numSamples];
    QVector<float> channelData;
    channelData.resize(numSamples);

    audio[0]  = channelData.data();

    float*channel0 = audio[0];
    for (int z=0;z<numSamples;z++) {
       channel0[z]=static_cast<float>(audioData.at(z));
    }

    mFilterBandPass->process (numSamples, audio);

    /* Envelope */

    for (int z=0;z<numSamples;z++) {
        channel0[z]=qAbs<float>(channel0[z]);
    }
#if 1
    mFilterLowPass->process(numSamples, audio);
#endif
    QVector<qint16> ret;
    ret.resize(numSamples);

    for (int z=0;z<numSamples;z++) {
        ret[z]=channel0[z];
    }

    emit newAudioDataReceived(ret);
    }
}

void AudioInterface::onAudioOutputstateChanged(QAudio::State state)
{
#if 0
    /* We get underrun error once samples have been played and no new
     * data is added. */
    qDebug() << state << "error:" << m_audioOutput->error();
#endif
}

void AudioInterface::setupAudioFormat()
{
    QList<int> rates = QAudioDeviceInfo::defaultOutputDevice().supportedSampleRates();

    int sampleRate=0;
    for ( auto rate : rates ) {
        if ( rate >40000) {
            sampleRate = rate;
            break;
        }
    }

    qDebug() << "Using samplerate " << sampleRate;

    m_format.setSampleRate(sampleRate);
    m_format.setChannelCount(1);
    m_format.setSampleSize(audioSampleSizeInBits);
    QString codec = QAudioDeviceInfo::defaultOutputDevice().supportedCodecs().first();
    qDebug() << "Using codec " << codec;
    m_format.setCodec(codec); // "audio/pcm"
    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setSampleType(QAudioFormat::SignedInt);
}

void AudioInterface::initializeAudio()
{
    setupAudioFormat();

    QAudioDeviceInfo ouputInfo(m_outputDevice);
    if (!ouputInfo.isFormatSupported(m_format)) {
        qWarning() << "Default format not supported - trying to use nearest";
        m_format = ouputInfo.nearestFormat(m_format);
    }

    /* Setup output */
    if (m_outputCarriedGenerator) {
        delete m_outputCarriedGenerator;
    }
    m_outputCarriedGenerator = new CarrierGenerator(m_format,
                                       DurationSeconds*1000000,
                                       CarrierSampleRateHz, this);
    createAudioOutput();

    /* Input */
    qDebug() <<"input device: "<< m_inputDevice.deviceName();

    if (m_inputAudioDataReceiver) {
        delete m_inputAudioDataReceiver;
    }
    m_inputAudioDataReceiver  = new AudioRecorder(m_format, this);
    createAudioInput();

    /* Setup filter */

    // Create a Chebyshev type I Band Stop filter of order 3
    // with state for processing 2 channels of audio.
    //    Dsp::SimpleFilter <Dsp::ChebyshevI::BandStop <3>, 2> f;
    //    f.setup (3,    // order
    //             44100,// sample rate
    //             4000, // center frequency
    //             880,  // band width
    //             1);   // ripple dB
    //    f.process (numSamples, arrayOfChannels);

    mFilterBandPass= new Dsp::SmoothedFilterDesign
      <Dsp::Butterworth::Design::BandPass <4>, 1, Dsp::DirectFormII> (dspFilterSampleSize);
    Dsp::Params params;
    params[0] = m_format.sampleRate(); // sample rate
    params[1] = 4; // order
    params[2] = PayloadCarrierSampleRateHz; // center frequency
    params[3] = PayloadCarrierSampleRateHz*1/1.5; // band width
    mFilterBandPass->setParams (params);


    mFilterLowPass = new Dsp::SmoothedFilterDesign
      <Dsp::RBJ::Design::LowPass, 1> (dspFilterSampleSize);
    params[0] =  m_format.sampleRate(); // sample rate
    params[1] = PayloadCarrierSampleRateHz*1/10.0; // cutoff frequency
    params[2] = 1.0; // Q
    mFilterLowPass->setParams (params);

    emit audioSetupDone(m_format);
}

void AudioInterface::createAudioOutput()
{
    qDebug() << __FUNCTION__;

    delete m_audioOutput;
    m_audioOutput = 0;
    m_audioOutput = new QAudioOutput(m_outputDevice, m_format, this);
    //m_audioOutput->setNotifyInterval(audioNotifyIntevalTargetMs);

    m_audioOutput->setVolume(1.0);
    //m_audioOutput->setBufferSize(m_format.sampleRate()*m_format.bytesPerFrame()*m_format.channelCount()/bufferSizeDivisor);
    m_audioOutput->setBufferSize(256*m_format.bytesPerFrame()*m_format.channelCount());
    //m_audioOutput->setBufferSize(m_format.framesForDuration(audioTimerIntervalMs*1000)*m_format.bytesPerFrame()*m_format.channelCount());

    m_outputCarriedGenerator->start(); // open
    m_audioOutput->start(m_outputCarriedGenerator);

    qDebug() << " Used tx audio buffersize is " << m_audioOutput->bufferSize() << "periodsized " << m_audioOutput->periodSize();

    connect(m_audioOutput,&QAudioOutput::notify,
            this,&AudioInterface::onTxAudioNotify);

    connect(m_audioOutput,&QAudioOutput::stateChanged,
            this,&AudioInterface::onAudioOutputstateChanged);

}

void AudioInterface::createAudioInput()
{
    delete m_audioInput;
    m_audioInput=0;
    m_audioInput = new QAudioInput(m_inputDevice, m_format, this);
    m_audioInput->setNotifyInterval(audioNotifyIntevalTargetMs);

    //m_audioInput->setBufferSize(m_format.sampleRate()*m_format.bytesPerFrame()*m_format.channelCount()/bufferSizeDivisor);
    m_audioInput->setBufferSize(1280*m_format.bytesPerFrame()*m_format.channelCount());

    m_inputAudioDataReceiver->start();
    m_audioInput->start(m_inputAudioDataReceiver);

    qDebug() << " Used rx audio buffersize is " << m_audioInput->bufferSize();

    connect(m_audioInput,&QAudioInput::notify,
            this, &AudioInterface::onRxAudioNotify);

    connect(m_inputAudioDataReceiver,&AudioRecorder::newAudioDataReceived,
            this,&AudioInterface::onNewAudioDataReceived);

    m_audioInput->start(m_inputAudioDataReceiver);

}
