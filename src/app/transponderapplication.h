#ifndef TRANPONDERAPPLICATION_H
#define TRANPONDERAPPLICATION_H

#include <QObject>
#include <QTimer>
#include <QThread>

#include "audiointerface.h"
#include "audiomodem.h"

class TransponderApplication : public QObject
{
    Q_OBJECT
public:
    explicit TransponderApplication(QObject *parent = nullptr);

    Q_INVOKABLE void startAudio();

    Q_INVOKABLE void setStationMode();
    Q_INVOKABLE void setResponderMode(int responderId);

    Q_INVOKABLE void quitApplication();

signals:
    void newAudioOutputDataAvailable(QList<qreal> datapoints);
    void newAudioInputDataAvailable(QList<qreal> datapoints);
    void newAudioTobeProcessedDataAvailable(QList<qreal> datapoints);
    void newMeasuredRxLevel(int rxLevel);
    void newDistanceMeasure(qreal distanceMeters);
public slots:

private slots:
    void onNewAudioDataProcessed(QVector<qint16> audioData);
    void onNewAudioDataReceived(QVector<qint16> audioData);
    void onNewAudioDataTransmitted(QVector<qint16> audioData);
    void onVisualizeTimerTimeOut();
    void onAudioSetupDone(QAudioFormat audioFormat);
private:
    AudioInterface *mAudio;
    AudioModem *mModem;

    struct audioLevelTag {
        qreal minLevel;
        qreal maxLevel;
    } mRxLevels;

    bool mRxVizRecordingEnabled;

    audioLevelTag mTxLevels;

    QList<qreal> mNormalizedRxData;
    quint64 mSampleSkipCount;
    QElapsedTimer mDisplayUpdateIntervalForInputData;

    bool mIsResponderMode;

    QAudioFormat mCurrentAudioFormat;

    QTimer mVisualizerTimer;


    QThread mAudioWorker;
    QThread mModemWorker;
    void resetRxVisualization();
};

#endif // TRANPONDERAPPLICATION_H
