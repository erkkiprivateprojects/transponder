#ifndef AUDIOMODEM_H
#define AUDIOMODEM_H

#include <QObject>
#include <QVector>
#include <QAudioFormat>
#include <QTimer>
#include <QElapsedTimer>

class AudioModem : public QObject
{
    Q_OBJECT
public:
    explicit AudioModem(QObject *parent = nullptr);

signals:
    void sendAudioData(QVector<qint16> audioData);

    void startedProcessingAudioData(QVector<qint16> audioData);

    void currentRxLevelDbChanged(int dbVal);
    void newDistanceMeasure(qreal distanceMeters);

public slots:
    void onInit();

    void onNewAudioDataReceived(QVector<qint16> audioData);
    void onAudioSetupDone(QAudioFormat audioFormat);

    void startPinging();
    void stopPinging();

    void startResponding(int);
    void stopResponding();




private slots:
    void onTimeOut();

    quint64 messageLenghtInUs(QList<qint16> &msg);
private:
    void sendMessage(quint8 messageCharacter);

    QVector<qint16> generateSignalling(QVector<bool> bitStream);

    QByteArray processPossibleMessage(QList<qint16> &msg);

    struct audioLevelTag {
        qreal minLevel;
        qreal maxLevel;
    } mRxLevels;

    audioLevelTag mRxLevelsCandidate;


    bool mPingModeOn;
    bool mRespondModeOn;

    quint64 rxAudioTimeUs;

    QElapsedTimer mRxLevelUpdateInterval;
    int mMaxRxLevel;

    QList<qint16> mRxMessageSignalBlock;
    bool mRecordMessage;
    quint64 mTimeOfLastEdgeUs;
    quint64 mTimeOfLastTx;
    quint64 mTimeOfLastRx;

    QByteArray mLastSentMessage;
    QTimer *mTimer;
    QAudioFormat mCurrentAudioFormat;

    qreal calculateDb(qint16 signalLevel);

    QVector<qint16> generateData(const QAudioFormat &format, qint64 durationUs, qreal inputSignalFrequency);
    void sendAudio(QVector<qint16> data, quint64 currentTimeUs);
    qreal getBitTimeMs();
    quint64 getSampleTimeUs();
    bool handleRx(QList<qint16> &rxMessageBlock, quint64 timeOfLastEdgeUs);
    void sendPing();
};

#endif // AUDIOMODEM_H
