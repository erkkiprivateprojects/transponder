#include <qmath.h>
#include <qendian.h>
#include <QDebug>

#include "carriergenerator.h"
#include "audioconfiguration.h"

CarrierGenerator::CarrierGenerator(const QAudioFormat &format, qint64 durationUs, int sampleRate, QObject *parent):QIODevice(parent),
    mCurrentFormat(format)
{
}

void CarrierGenerator::start()
{
    open(QIODevice::ReadOnly);
    mReadInterval.start();
}

void CarrierGenerator::stop()
{
    close();
}

qint64 CarrierGenerator::readData(char *data, qint64 len)
{
    int readLen = len;
    qint64 ret =0;    
    /* Mix possible tx data to output */
    if (mAudioToBeMixed.size()>0 ) {
        mDataBlock.resize(readLen);
        mDataBlock.fill(0x00,mDataBlock.size());
        mDataBlock = performAudioMix(mDataBlock.size());
        memcpy(data, mDataBlock.data(), mDataBlock.size());
        mReadInterval.start();
        ret = mDataBlock.size();
    }
    else {
    }

#if 0
     qDebug () << "read"  << len << "returned" << ret;
#endif
    return ret;
}

qint64 CarrierGenerator::writeData(const char *data, qint64 len)
{
    Q_UNUSED(data);
    Q_UNUSED(len);

    return 0;
}

qint64 CarrierGenerator::bytesAvailable() const
{
    qDebug() << __FUNCTION__;
    return m_buffer.size() + QIODevice::bytesAvailable();
}

void CarrierGenerator::sendAudioData(QVector<qint16> audioData)
{
    mAudioToBeMixed.append(audioData);
}

QByteArray CarrierGenerator::performAudioMix(int bufferSize)
{
    /* It is assumed that both streams are 1 channel, littleendian, signed 16 bit with common samplerate */
    QByteArray ret;
    int amountOfSamplesAvailableForMix = mAudioToBeMixed.size();
    int amountOfSamples =bufferSize/mCurrentFormat.bytesPerFrame();

    int amountOfSamplesToMix = qMin<int>(amountOfSamplesAvailableForMix,amountOfSamples);

    int targetBufferSize = amountOfSamplesToMix*mCurrentFormat.bytesPerFrame()*mCurrentFormat.channelCount();

    ret.resize(targetBufferSize);

    qint64 samplesAdded = 0;
    unsigned char *ptr = reinterpret_cast<unsigned char *>(ret.data());
    for (int z=0;z<amountOfSamplesToMix;z++) {
        if ( z <mAudioToBeMixed.size() ) {
            qint16 value = mAudioToBeMixed.at(z);
            qToLittleEndian<qint16>(value, ptr);
            samplesAdded++;
        }
        else {
            break;
        }
        ptr+=mCurrentFormat.bytesPerFrame();
    }

    /* remove already mixed data */
    mAudioToBeMixed.remove(0,samplesAdded);

    return ret;
}
