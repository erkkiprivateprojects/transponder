#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQmlEngine>
#include <QVector>

#include "transponderapplication.h"
#include "audioscope.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qRegisterMetaType<QVector<qint16> >("QVector<qint16>");
    qmlRegisterType<TransponderApplication>("Transponder",1,0,"TransponderApplication");
    qmlRegisterType<AudioScope>("Transponder",1,0,"AudioScope");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
