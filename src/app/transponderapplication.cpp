#include <QDebug>
#include <QMetaObject>
#include "transponderapplication.h"

TransponderApplication::TransponderApplication(QObject *parent) : QObject(parent), mAudio(NULL),
    mRxLevels({0}),mRxVizRecordingEnabled(false),mSampleSkipCount(0),mIsResponderMode(false)
{
    mAudio = new AudioInterface(NULL);
    mModem = new AudioModem(NULL);

    mAudio->moveToThread(&mAudioWorker);
    mAudioWorker.start(QThread::TimeCriticalPriority);

    mModem->moveToThread(&mModemWorker);
    mModemWorker.start(QThread::HighPriority);

    connect(mAudio,&AudioInterface::newAudioDataReceived,
            mModem,&AudioModem::onNewAudioDataReceived);

    connect(mAudio,&AudioInterface::newAudioDataReceived,
            this,&TransponderApplication::onNewAudioDataReceived);

    connect(mAudio,&AudioInterface::audioSetupDone,
            mModem,&AudioModem::onAudioSetupDone);

    connect(mAudio,&AudioInterface::audioSetupDone,
            this,&TransponderApplication::onAudioSetupDone);

    connect(mModem,&AudioModem::sendAudioData,
            mAudio,&AudioInterface::onSendAudioData);

    connect(mModem,&AudioModem::startedProcessingAudioData,
            this,&TransponderApplication::onNewAudioDataProcessed);

    connect(mModem,&AudioModem::sendAudioData,
            this, &TransponderApplication::onNewAudioDataTransmitted);

    connect(mModem,&AudioModem::currentRxLevelDbChanged,
            this, &TransponderApplication::newMeasuredRxLevel);

    connect(mModem,&AudioModem::newDistanceMeasure,
            this, &TransponderApplication::newDistanceMeasure);

    connect(&mVisualizerTimer,&QTimer::timeout,
            this,&TransponderApplication::onVisualizeTimerTimeOut);

    QMetaObject::invokeMethod(mModem,"onInit",Qt::QueuedConnection);

    mVisualizerTimer.start(1000);
    mDisplayUpdateIntervalForInputData.start();
}

void TransponderApplication::startAudio() {
    QMetaObject::invokeMethod(mAudio,"initAudio");
}

void TransponderApplication::setStationMode()
{
    mIsResponderMode=false;
    resetRxVisualization();
    emit newAudioOutputDataAvailable(mNormalizedRxData);

    QMetaObject::invokeMethod(mModem,"startPinging");
}

void TransponderApplication::setResponderMode(int responderId)
{
    mIsResponderMode=true;
    resetRxVisualization();
    emit newAudioOutputDataAvailable(mNormalizedRxData);

    QMetaObject::invokeMethod(mModem,"startResponding",Q_ARG(int,responderId));
}

void TransponderApplication::quitApplication()
{
    mAudioWorker.quit();
    mModemWorker.quit();

    while(mAudioWorker.isFinished()==false ) {
        QThread::msleep(1);
    }
    while(mModemWorker.isFinished()==false ) {
        QThread::msleep(1);
    }
}


void TransponderApplication::onNewAudioDataReceived(QVector<qint16> audioData)
{
    /* for visualization purposes */
    if (mRxVizRecordingEnabled==true) {

        for (auto i : audioData) {
            qreal v = i;
            mRxLevels.maxLevel = qMax<qreal>(mRxLevels.maxLevel,v);
            mRxLevels.minLevel = qMin<qreal>(mRxLevels.minLevel,v);
        }

        qreal amplitudeMax = qMax<qreal>(qAbs<qreal>(mRxLevels.maxLevel),qAbs<qreal>(mRxLevels.minLevel));

        const qreal maxRangeM = 150; // range we expect signal fly to remote

        const qreal maxProcessingTimeOnRemoteMs = 5; // time that remote handles response
        qreal maxTimeToVisualizeAfterTriggerMs =maxProcessingTimeOnRemoteMs+ (maxRangeM*2 / 330)*1000.0;

        maxTimeToVisualizeAfterTriggerMs = 800;

        for (auto i : audioData) {
            qreal n = static_cast<qreal>(i)/amplitudeMax;

            if ( n<=1.0) {
                mNormalizedRxData.append(n);
            }
        }

        if (mNormalizedRxData.size()>0 ){
            // indicate frameposition
            mNormalizedRxData.replace(mNormalizedRxData.size()-1,-0.1);
        }

        int maxWindowMs =3000;
            int maxSampleCount = maxWindowMs/1000.0 *
                    static_cast<qreal>(mCurrentAudioFormat.sampleRate());
            while ( mNormalizedRxData.size()>maxSampleCount) {
                mNormalizedRxData.takeFirst();
            }
            if (mDisplayUpdateIntervalForInputData.elapsed()>1000){
                emit newAudioInputDataAvailable(mNormalizedRxData);
                mDisplayUpdateIntervalForInputData.restart();
            }
    }
}

void TransponderApplication::onNewAudioDataTransmitted(QVector<qint16> audioData)
{
    if ( mIsResponderMode==false) {
       resetRxVisualization();
    }

    /* for visualization purposes */
    QList<qreal> normalized;

    for (auto i : audioData) {
        qreal v = i;
        mTxLevels.maxLevel = qMax<qreal>(mTxLevels.maxLevel,v);
        mTxLevels.minLevel = qMin<qreal>(mTxLevels.minLevel,v);
    }
    qreal nMax = qMax<qreal>(qAbs<qreal>(mTxLevels.maxLevel),qAbs<qreal>(mTxLevels.minLevel));

    for (auto i : audioData) {
        qreal n = static_cast<qreal>(i)/nMax;
        normalized.append(n);
    }

    emit newAudioOutputDataAvailable(normalized);

}

void TransponderApplication::onNewAudioDataProcessed(QVector<qint16> audioData)
{
    /* for visualization purposes */
    QList<qreal> normalized;

    qreal minValue = 0;
    qreal maxValue = 0;
    for (auto i : audioData) {
        qreal v = i;
        maxValue = qMax<qreal>(maxValue,v);
        minValue= qMin<qreal>(minValue,v);
    }
    qreal nMax = qMax<qreal>(qAbs<qreal>(maxValue),qAbs<qreal>(minValue));
    for (auto i : audioData) {
        qreal n = static_cast<qreal>(i)/nMax;
        normalized.append(n);
    }

    emit newAudioTobeProcessedDataAvailable(normalized);
}


void TransponderApplication::resetRxVisualization() {
//    mRxLevels.maxLevel=0;
//    mRxLevels.minLevel=0;
    mRxVizRecordingEnabled = true;
}

void TransponderApplication::onVisualizeTimerTimeOut()
{
    qreal decay=0.99;
    mRxLevels.maxLevel*=decay;
    mRxLevels.minLevel*=decay;
    mTxLevels.maxLevel*=decay;
    mTxLevels.minLevel*=decay;
}

void TransponderApplication::onAudioSetupDone(QAudioFormat audioFormat)
{
    mCurrentAudioFormat = audioFormat;
}
