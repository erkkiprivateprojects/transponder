#ifndef AUDIOCONFIGURATION_H
#define AUDIOCONFIGURATION_H

const int DurationSeconds = 1.0;
const int DataSampleRateHz = 44100;
const int CarrierSampleRateHz = 1000;//600;
const qreal CarrierAmplitudeN = 0.001;
const int PayloadCarrierSampleRateHz=DataSampleRateHz/8;//DataSampleRateHz/10;
const int audioSampleSizeInBits = 16;
const int audioTimerIntervalMs = 10; // notify time won't work if value is too small
const int audioNotifyIntevalTargetMs = audioTimerIntervalMs*2; // tries to use this

static quint64 periodsForBit =100;

#endif // AUDIOCONFIGURATION_H
