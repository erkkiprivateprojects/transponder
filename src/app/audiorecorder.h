#ifndef AUDIORECORDER_H
#define AUDIORECORDER_H

#include <QObject>
#include <QIODevice>
#include <QByteArray>
#include <QAudioFormat>

class AudioRecorder : public QIODevice
{
    Q_OBJECT
public:
    AudioRecorder(const QAudioFormat &format, QObject *parent);

    void start();
    void stop();

    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;

signals:
    void newAudioDataReceived(QVector<qint16> audioData);
public slots:

private:
    const QAudioFormat m_format;
};

#endif // AUDIORECORDER_H
