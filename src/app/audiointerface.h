#ifndef AUDIOINTERFACE_H
#define AUDIOINTERFACE_H

#include <QObject>
#include <QAudioDeviceInfo>
#include <QAudioOutput>
#include <QAudioInput>
#include <QTimer>
#include <QByteArray>
#include <QVector>
#include <QDateTime>

#include "carriergenerator.h"
#include "audioconfiguration.h"
#include "audiorecorder.h"
#include "DspFilters/Dsp.h"

static const int dspFilterSampleSize = periodsForBit/2;

class AudioInterface : public QObject
{
    Q_OBJECT
public:
    AudioInterface(QObject *parent);


signals:

    void newAudioDataReceived(QVector<qint16> audioData);

    void audioSetupDone(QAudioFormat audioFormat);

public slots:
    void onSendAudioData(QVector<qint16> audioData);
    bool initAudio();

private slots:
    void deviceChanged();
    void volumeChanged(int value);
    void onRxAudioNotify();
    void onTxAudioNotify();
    void onNewAudioDataReceived(QVector<qint16> audioData);
    void onAudioOutputstateChanged(QAudio::State state);
private:
    void initializeAudio();
    void createAudioOutput();
    void createAudioInput();

    QAudioDeviceInfo m_outputDevice;
    QAudioDeviceInfo m_inputDevice;

    QTimer *m_pushTimer;
    QAudioFormat m_format;

    // audio output
    QAudioOutput *m_audioOutput;
    QIODevice *m_output; // not owned
    CarrierGenerator *m_outputCarriedGenerator;

    // audio input
    QAudioInput  *m_audioInput;
    AudioRecorder *m_inputAudioDataReceiver;

    void setupAudioFormat();

    quint64 mRxAudioTimeMs;
    quint64 mTxAudioTimeMs;
    QDateTime mRxAudioWallTime;
    QDateTime mTxAudioWallTime;

    Dsp::Filter* mFilterBandPass;
    Dsp::Filter* mFilterLowPass;
    float EnvIn;

};

#endif // AUDIOINTERFACE_H
