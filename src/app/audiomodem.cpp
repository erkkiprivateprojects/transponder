#include <QDebug>
#include <QtMath>
#include <QtEndian>
#include <math.h>
#include "audiomodem.h"
#include "audioconfiguration.h"

static quint64 responsePulseWidthMs = 300;
static quint64 requestPulseWidthMs=450;
static quint64 timeBetweenRequestAndResponseMs = 50;

static const int defaultDistancePollIntervalMs = 1000;


AudioModem::AudioModem(QObject *parent) : QObject(parent),
    rxAudioTimeUs(0),
    mRxLevels({0}),
    mPingModeOn(false),
    mRespondModeOn(false),
    mRecordMessage(0),
    mMaxRxLevel(-1000)
{

}

void AudioModem::onInit()
{
    mTimer = new QTimer;
    connect(mTimer,&QTimer::timeout,
            this,&AudioModem::onTimeOut);

    mTimer->start(defaultDistancePollIntervalMs);
    mRxLevelUpdateInterval.start();
}

quint64 AudioModem::getSampleTimeUs() {
    return (1.0/static_cast<qreal>(mCurrentAudioFormat.sampleRate()))*1000000.0;
}

bool AudioModem::handleRx(QList<qint16> &rxMessageBlock, quint64 timeOfLastEdgeUs)
{
    bool ret = false; // if true, means that we just heard message we sent
    QByteArray rxMsg =
            processPossibleMessage(rxMessageBlock);
    if ( rxMsg.length()>0) {
        qDebug() <<"responder rx: " << rxMsg.toHex();
        mTimeOfLastRx=timeOfLastEdgeUs;
        if ( mRespondModeOn==true) {

            mTimeOfLastRx=timeOfLastEdgeUs;
            if (rxMsg.at(0)==0x55) {
                /* Send response */
                sendMessage(static_cast<quint8>(0xf0));
            }
        }
        else {
            qDebug() <<"station rx: " << rxMsg.toHex() << " : " << mLastSentMessage.toHex();
            if (rxMsg==mLastSentMessage || rxMsg.at(0)==0x55){
                /* Transmitter hears own signal */
                qDebug() <<   mTimeOfLastTx/1000.0;
                mTimeOfLastTx=timeOfLastEdgeUs;
                qDebug() << "last txtime="<<mTimeOfLastTx;
                ret = true;
            }
            if (static_cast<quint8>(rxMsg.at(0))==static_cast<quint8>(0x07) ) {
                qDebug() << "tx:" << mTimeOfLastTx/1000.0 << "rx:"<< mTimeOfLastRx/1000.0;

                if (mTimeOfLastRx>mTimeOfLastTx) {
                    qDebug () << "response from responder";
                    qreal d= (mTimeOfLastRx-mTimeOfLastTx)/1000.0;
                    emit newDistanceMeasure(d);
                }
            }
        }
    }
    return ret;
}

void AudioModem::onNewAudioDataReceived(QVector<qint16> audioData)
{
    //qDebug() << __FUNCTION__ << audioData.size()/static_cast<qreal>(mCurrentAudioFormat.sampleRate())*1000.0;
    /* for visualization purposes */
    quint64 currentTimeUs = rxAudioTimeUs;
    quint64 sampleTimeUs = getSampleTimeUs();
    for (int z =0;z<audioData.size();z++) {
        auto i = audioData.at(z);
        currentTimeUs+=sampleTimeUs;

        bool startAnalysis=false;

        int amountOfBits = 10 + 1; //1 start, 8 data, 1 stop +  1 for creating start bit edge
        if (currentTimeUs-mTimeOfLastEdgeUs >= amountOfBits* getBitTimeMs()*1000.0) {
            /* After one character silence, we stop recording and
                 * try analyze */
            if ( mRecordMessage==true) {
                mRecordMessage =false;
                startAnalysis = true;
            }
        }

        if ( mRecordMessage==true) {
            mRxMessageSignalBlock.append(i);
        }

        if (startAnalysis==true) {
            /* Analyze this */
            if (mRxMessageSignalBlock.size()>0 ){
                qDebug() << "analyze" <<mRxMessageSignalBlock.size();
                handleRx(mRxMessageSignalBlock,mTimeOfLastEdgeUs);
            }
        }

#ifdef _WIN32
        const int minRxLevelDB = -40
                ;
#else
        const int minRxLevelDB = -40;
#endif
        mMaxRxLevel = qMax<int>(mMaxRxLevel,calculateDb(i)) ;

        if( calculateDb(i)>minRxLevelDB) {
            if ( mRecordMessage==false) {
                mTimeOfLastEdgeUs = currentTimeUs;
                //qDebug() << "start recording " << "dbvalue " << calculateDb(i);
                mRxMessageSignalBlock.clear();
                mRecordMessage=true;
                mRxLevelsCandidate = mRxLevels;
            }
        }
    }

    if ( mRxLevelUpdateInterval.elapsed()>1000) {

        emit currentRxLevelDbChanged(mMaxRxLevel);
        mMaxRxLevel=-1000;
        mRxLevelUpdateInterval.restart();
    }

    rxAudioTimeUs = currentTimeUs;
}

void AudioModem::onAudioSetupDone(QAudioFormat audioFormat)
{
    mCurrentAudioFormat = audioFormat;
}

void AudioModem::startPinging()
{

    mPingModeOn = true;
    mRespondModeOn=false;
}

void AudioModem::stopPinging()
{
    mPingModeOn = false;
}

void AudioModem::startResponding(int /*stationId*/)
{
    mPingModeOn = false;
    mRespondModeOn=true;

}

void AudioModem::stopResponding()
{
    mRespondModeOn = false;
}

void AudioModem::sendAudio(QVector<qint16> data, quint64 currentTimeUs)
{
    emit sendAudioData(data);
}

void AudioModem::sendPing()
{
    sendMessage(static_cast<quint8>(0x55));
    mTimeOfLastTx=rxAudioTimeUs;
}

void AudioModem::onTimeOut()
{
    /* Generate test pulse */
    if ( mPingModeOn==true) {
        if (mCurrentAudioFormat.isValid()==true) {
            if ( mRecordMessage==false) {
                sendPing();
            }
        }
    }
}

void AudioModem::sendMessage(quint8 messageCharacter)
{
    QByteArray message;
    message.append(messageCharacter);

    mLastSentMessage = message;
    /* Now only short messages can be set because bit clock is synced
     * in start of firt bit. Clock drifts too much if message is longer than one byte */
    QVector<bool> bitStream;

    qDebug()<< "Tx: " << message.toHex();

    for ( int byteIndex = 0; byteIndex<message.size();byteIndex++) {

        quint8 byte = message.at(byteIndex);

        /* Add start bit. This is needed for start of frame detection and
         * end of transmission (silence after message ) */
        bitStream.append(true);

        for ( uint bitIndex = 0; bitIndex<sizeof(quint8)*8;bitIndex++) {
            quint8 bitValue = byte >> bitIndex & 0x01;
            bool bit;
            if (bitValue>0) {
                bit=true;
            }
            else {
                bit=false;
            }
            bitStream.append(bit);
        }

        /* Add stop bit */
        bitStream.append(true);
    }

    QVector<qint16> audioMessage;
    audioMessage = generateSignalling(bitStream);
    sendAudio(audioMessage,rxAudioTimeUs);
}

qreal AudioModem::getBitTimeMs()
{
    qreal bitTimeMs;
    bitTimeMs=static_cast<qreal>(periodsForBit)*1.0/PayloadCarrierSampleRateHz*1000.0;
    return bitTimeMs;
}

QVector<qint16> AudioModem::generateSignalling(QVector<bool> bitStream)
{
    QVector<qint16> ret;
    qreal bitTimeMs;
    bitTimeMs = getBitTimeMs();

    QVector<qint16>  bitPulse = generateData(mCurrentAudioFormat,
                                             bitStream.size()*bitTimeMs*1000.0,
                                             PayloadCarrierSampleRateHz);

    int samplesPerBit = bitPulse.size()/bitStream.size();

    for (int z = 0; z<bitStream.size();z++) {

        qreal amplitudeN = 1.0;

        bool bit = bitStream.at(z);

        if ( bit==true) {
            amplitudeN=1.0;
        }
        else {
            amplitudeN=0.1;
        }

        int offset = z * samplesPerBit;
        for (int y=0;y<samplesPerBit;y++) {
            bitPulse[offset+y]*=amplitudeN;
        }
    }

    ret=bitPulse;
    return ret;
}

quint64 AudioModem::messageLenghtInUs(QList<qint16> &msg) {
    quint64 ret=0;

    quint64 sampleTimeUs = getSampleTimeUs();

    for ( auto i : msg) {
        ret+=sampleTimeUs;
    }

    return ret;
}

QByteArray AudioModem::processPossibleMessage(QList<qint16> &msg)
{
    QByteArray ret;

    qint16 maxValue=0;
    qint16 halfValue=0;
    QVector<qint16> block;
    block.resize(msg.size());
    for (int z=0;z<msg.size();z++) {
        block[z]=msg.at(z);
        maxValue=qMax<qint16>(maxValue,msg.at(z));
    }

    halfValue = maxValue/2;

    /* Calculate bit sample time. Each byte have 2 extra bits */
    quint64 singleBitTimeUs = (getBitTimeMs()*1000.0);

    quint64 payloadMessageLengtUs = (messageLenghtInUs(msg));
    quint64 payloadBitCount = (payloadMessageLengtUs/singleBitTimeUs);

    if ( payloadBitCount>0) {
        quint64 samplingIntervalUs = getBitTimeMs()*1000.0;

        quint64 sampleInterval = (samplingIntervalUs/getSampleTimeUs());

        quint64 samplingStartPos =sampleInterval/2;

        qDebug() << "bitcount" << payloadBitCount << payloadMessageLengtUs/1000.0<<samplingIntervalUs/1000.0;

        QList<bool> bitStream;
        for (quint64 sampleIndex =samplingStartPos;sampleIndex<msg.size();sampleIndex+=sampleInterval ){
            qint16 v = msg.at(sampleIndex);
            bool bit = false;
            if ( v>halfValue) {
                bit=true;
            }
            else {
                bit=false;
            }

            block[sampleIndex]=0; // mark bit sample position

            bitStream.append(bit);
        }

        emit startedProcessingAudioData(block);

        /* Convert bitstream to QbyteArray */
        quint8 byte=0;

        int byteBitIndex=0;
        QString msgBits;
        QString msgPayloadBits;
        int processedBits=0;
        bool invalidMessage = false;
        for ( auto bit : bitStream) {

            if (bit==true) {
                msgBits.append("1");
            }
            else {
                msgBits.append("0");
            }

            if ( byteBitIndex==0) {
                byteBitIndex++; // skip start bit
                if ( bit==false) {
#if 0
                    qDebug() <<"Incorrect start bit" << bit;
#endif
                    invalidMessage = true;
                    break;
                }
            }
            else if (byteBitIndex==8 ) {
                ret.append(byte);
                byte=0;
                byteBitIndex++;
            }
            else if (byteBitIndex==9 ) {
                byteBitIndex=0; // skip stop bit
                if ( bit==false) {
#if 0
                    qDebug() <<"Incorrect stop bit" << bit;
#endif
                    invalidMessage = true;
                    break;
                }
            }
            else {
                if (bit==true) {
                    byte = byte<<1 | 0x01;
                    msgPayloadBits.append("1");
                }
                else {
                    byte = byte<<1 & 0xfe;
                    msgPayloadBits.append("0");
                }
                byteBitIndex++;

            }
            processedBits++;
            if ( processedBits==payloadBitCount) {
                break; // skip trailers (silent bits)
            }
        }

        qDebug() << "msg:" <<msgBits<< " payload: " << msgPayloadBits << ret.toHex();
    }
    return ret;
}

qreal AudioModem::calculateDb(qint16 signalLevel)
{
    qreal ret = 0;
    qreal ref = (signalLevel+1)/static_cast<qreal>(SHRT_MAX);

    ret =  20 * log10(qAbs<qreal>(ref));

    return ret;
}

QVector<qint16> AudioModem::generateData(const QAudioFormat &format, qint64 durationUs, qreal inputSignalFrequency)
{
    /* Generate always signed 16 bit audio data in little endian format */
    QVector<qint16> ret;

    const int channelBytes = format.sampleSize() / 8;

    qint64 length = format.sampleRate() * durationUs / 1000000;
    ret.resize(length);
    unsigned char *ptr = reinterpret_cast<unsigned char *>(ret.data());
    for ( int sampleIndex =0; sampleIndex < length;sampleIndex++) {

        const qreal x = qSin(2 * M_PI * inputSignalFrequency  * qreal(sampleIndex % format.sampleRate()) / format.sampleRate());

        qint16 value = static_cast<qint16>(x * 32767);

        if (format.byteOrder() == QAudioFormat::LittleEndian) {
            qToLittleEndian<qint16>(value, ptr);
        }
        else {
            qToBigEndian<qint16>(value, ptr);
        }
        ptr+=channelBytes;
    }

    return ret;
}
