import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: root

    signal configStationModeRequested
    signal configResponderModeRequested

    Rectangle {
        id:rootBox
        anchors.fill: parent
        color: "gray"

        Column {

            spacing: 20
            anchors.centerIn: parent

            Button {
                text: "I am responder"
                onClicked: {
                    configResponderModeRequested()
                }
            }

            Button {
                text: "I am station"
                onClicked: {
                    configStationModeRequested()
                }
            }

        }
    }
}
