import QtQuick 2.4

import Transponder 1.0

Item {
    id: root
    Rectangle {
        id:rootBox
        anchors.fill: parent

        color: "white"



        Column {

            anchors.fill: parent

            AudioScope {
                id: myAudioOutput

                measureName: "Output data"

                width: parent.width
                height: parent.height*1/4

                Connections {
                    target: myApp
                    onNewAudioOutputDataAvailable:{
                        myAudioOutput.onNewDrawingData(datapoints)
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        parent.viewIsUpdated=false
                    }
                    onReleased: {
                        parent.viewIsUpdated=true
                    }
                }
            }

            AudioScope {
                id: myAudioProcessInput

                Connections {
                    target: myApp
                    onNewAudioTobeProcessedDataAvailable:{
                        myAudioProcessInput.onNewDrawingData(datapoints)
                    }
                }

                measureName: "Input message process"

                width: parent.width
                height: parent.height*1/4

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        parent.viewIsUpdated=false
                    }
                    onReleased: {
                        parent.viewIsUpdated=true
                    }
                }
            }

            AudioScope {
                id: myAudioInput
                width: parent.width
                height: parent.height*2/4

                measureName: "Input data"

                Connections {
                    target: myApp
                    onNewAudioInputDataAvailable:{
                        myAudioInput.onNewDrawingData(datapoints)
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        parent.viewIsUpdated=false
                    }
                    onReleased: {
                        parent.viewIsUpdated=true
                    }
                }
            }
        }
        Text {
            id: rxLevelValue

            color:  "red"

            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: parent.height*1/16

            font.pointSize: 24

            Connections {
                target: myApp
                onNewMeasuredRxLevel:{
                    rxLevelValue.text = rxLevel
                }
            }
        }

        Text {
            id: distanceValue

            property int rxMeasuresReceived: 0

            anchors.right: parent.right
            anchors.top: rxLevelValue.bottom
            anchors.margins: parent.height*1/16

            font.pointSize: 24

            color:  "red"

            Connections {
                target: myApp
                onNewDistanceMeasure:{
                    distanceValue.text = distanceMeters+ "("+distanceValue.rxMeasuresReceived + ")"

                    distanceValue.rxMeasuresReceived++
                }
            }
        }
    }
}
