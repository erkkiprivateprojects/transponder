import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

import Transponder 1.0

ApplicationWindow  {
    visible: true
    width: 640
    height: 480
    title: qsTr("Transponder Application")

    TransponderApplication {
        id: myApp
    }

    onClosing: {
        myApp.quitApplication()
    }


    AppConfigureView {
        id: myConfig

        onConfigResponderModeRequested: {
            myApp.setResponderMode(1)
        }

        onConfigStationModeRequested: {
            myApp.setStationMode()
        }

        Button {
            text: "Apply"
            anchors.bottom:  parent.bottom
            anchors.right: parent.right
            anchors.margins: 16
            onClicked: {
                stack.pop()
            }
        }
    }

    TransponderView {
        id: myTransponder

        Button {
            text: "configure"
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 16
            onClicked: {
                stack.push(myConfig)
            }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent

    }

    Component.onCompleted: {
        myApp.startAudio()
        myApp.setStationMode()
        stack.push(myTransponder)
    }
}
