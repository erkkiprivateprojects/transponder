#include "audioscope.h"
#include <QDebug>
#include <QPainter>
#include <QFont>
#include <QTextOption>


#include "audioconfiguration.h"

AudioScope::AudioScope(QQuickPaintedItem *parent) : QQuickPaintedItem(parent),
    m_viewIsUpdated(true)
{
    setMeasureSampleRate(DataSampleRateHz);
}

void AudioScope::paint(QPainter *painter)
{
    int w = boundingRect().width();
    int h = boundingRect().height();

    if ( mAudioData.size()>0) {

        qreal xOffset = 0;
        qreal increment = w/static_cast<qreal>(mAudioData.size());

        if ( m_measureSampleRate>0) {
            qreal screenTimeMs = static_cast<qreal>(mAudioData.size())* (1.0/static_cast<qreal>(m_measureSampleRate))*1000.0;
            qreal msTimePerPixel = screenTimeMs /  w;

            qreal factor=1.0;
            qreal timeGridIncrement = w/10.0;
            while (msTimePerPixel*factor<timeGridIncrement){
                factor+=10.0;
            }

            //qDebug() << screenTimeMs << msTimePerPixel << factor;

            if ( msTimePerPixel>0) {
                QFont myFont;
                myFont.setPointSize(6);
                painter->setFont(myFont);

                for (int x = 0 ; x<w;x+=timeGridIncrement ) {
                    qreal timeMs =  static_cast<qreal>(x)*msTimePerPixel;
                    painter->drawLine(QLine(QPoint(x,0),QPoint(x,h)));

                    QRect box(boundingRect().toRect());
                    box.moveTopLeft(QPoint(x,h/16));
                    painter->drawText(box,QString("%1ms").arg(static_cast<qreal>(timeMs)),QTextOption());
                }
            }
        }
        painter->drawLine(QLine(QPoint(0,h/2),QPoint(w,h/2)));

        QLine l(QPoint(0,0),QPoint(0,0));

        bool startDrawing = true;
        if ( mAudioData.size()>0) {

            for (auto d : mAudioData) {

                if ( startDrawing==true) {
                    xOffset+=increment;
                }
                if ( xOffset>w) {
                    xOffset=0;

                    l = QLine(QPoint(0,0),QPoint(0,0));

                    painter->drawRect(boundingRect());
                }

                QPoint prev = l.p2();
                l.setP1(prev);

                int hValue = -1*d*h/2.0 + h/2.0;

                l.setP2(QPoint(xOffset,hValue));
                painter->drawLine(l);

            }
        }
    }

    QFont myFont;
    myFont.setPointSize(12);
    painter->setFont(myFont);

    QRect box(boundingRect().toRect());

    box.moveTopLeft(QPoint(h/16.0,h/16.0));

    painter->drawText(box,measureName(),QTextOption());
}

QString AudioScope::measureName() const
{
    return m_measureName;
}

int AudioScope::measureSampleRate() const
{
    return m_measureSampleRate;
}

void AudioScope::onNewDrawingData(QList<qreal> datapoints)
{
    if (m_viewIsUpdated==true) {
        mAudioData = datapoints;
        update();
    }
}

void AudioScope::setMeasureName(QString measureName)
{
    if (m_measureName == measureName)
        return;

    m_measureName = measureName;
    emit measureNameChanged(m_measureName);
}

void AudioScope::setMeasureSampleRate(int measureSampleRate)
{
    if (m_measureSampleRate == measureSampleRate)
        return;

    m_measureSampleRate = measureSampleRate;
    emit measureSampleRateChanged(m_measureSampleRate);
}
