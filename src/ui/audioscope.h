#ifndef AUDIOSCOPE_H
#define AUDIOSCOPE_H

#include <QObject>
#include <QQuickPaintedItem>

class AudioScope : public QQuickPaintedItem
{
    Q_OBJECT
public:
    explicit AudioScope(QQuickPaintedItem *parent = nullptr);

    void paint(QPainter *painter);

    Q_PROPERTY(bool viewIsUpdated READ viewIsUpdated WRITE setViewIsUpdated NOTIFY viewIsUpdatedChanged)

    Q_PROPERTY(QString measureName READ measureName WRITE setMeasureName NOTIFY measureNameChanged)

    Q_PROPERTY(int measureSampleRate READ measureSampleRate WRITE setMeasureSampleRate NOTIFY measureSampleRateChanged)

    QString measureName() const;

    int measureSampleRate() const;

    bool viewIsUpdated() const
    {
        return m_viewIsUpdated;
    }

signals:

    void measureNameChanged(QString measureName);

    void measureSampleRateChanged(int measureSampleRate);

    void viewIsUpdatedChanged(bool viewIsUpdated);

public slots:
    void onNewDrawingData(QList<qreal> datapoints);

    void setMeasureName(QString measureName);

    void setMeasureSampleRate(int measureSampleRate);

    void setViewIsUpdated(bool viewIsUpdated)
    {
        if (m_viewIsUpdated == viewIsUpdated)
            return;

        m_viewIsUpdated = viewIsUpdated;
        emit viewIsUpdatedChanged(m_viewIsUpdated);
    }

private:
    QList<qreal> mAudioData;

    QString m_measureName;
    int m_measureSampleRate;
    bool m_viewIsUpdated;
};

#endif // AUDIOSCOPE_H
