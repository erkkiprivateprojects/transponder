QT += quick multimedia
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/app/main.cpp \
    src/app/transponderapplication.cpp \
    src/app/audiointerface.cpp \
    src/app/carriergenerator.cpp \
    src/app/audiorecorder.cpp \
    src/app/audiomodem.cpp \
    src/ui/audioscope.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Bessel.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Biquad.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Butterworth.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Cascade.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/ChebyshevI.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/ChebyshevII.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Custom.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Design.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Documentation.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Elliptic.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Filter.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Legendre.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/Param.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/PoleFilter.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/RBJ.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/RootFinder.cpp \
    src/DSPFilters-master/shared/DSPFilters/source/State.cpp

RESOURCES += src/ui/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/app/transponderapplication.h \
    src/app/audiointerface.h \
    src/app/carriergenerator.h \
    src/app/audioconfiguration.h \
    src/app/audiorecorder.h \
    src/app/audiomodem.h \
    src/ui/audioscope.h

INCLUDEPATH+=src/app \
             src/ui \
             src/DSPFilters-master/shared/DSPFilters/include

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
